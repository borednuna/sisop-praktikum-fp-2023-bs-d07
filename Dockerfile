# Use the Ubuntu 20.04 base image
FROM ubuntu:20.04

# Set the working directory inside the container
WORKDIR /database_app

# Copy all directories and files into workdir
COPY client ./client
COPY database ./database
COPY dump ./dump

# Update the package manager and install any necessary dependencies
RUN apt-get update && apt-get install -y build-essential

# Compile the C programs
RUN gcc -o client_exe client/client.c
RUN gcc -pthread -o database_exe database/database.c

# Expose the necessary port for socket communication
EXPOSE 8080

# Run the database program
CMD [ "./database_exe" ]

# Set the entrypoint to run the compiled client program
ENTRYPOINT ["./client_exe"]
