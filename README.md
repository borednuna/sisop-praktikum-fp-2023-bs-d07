# Lapres Praktikum Final Kelompok D07
Anggota Kelompok ''D07'' 
| Nama                                    | NRP        |
|-----------------------------------------|------------|
| Hanun Shaka Puspa                       | 5025211051 |
| Mavaldi Rizqy Hazdi(Tidak berkontribusi)| 5025211086 |
| Daffa Saskara      (Tidak berkontribusi)| 5025201249 |

# Program Client 
Pada program client, digunakan _socket programming_ sebagai cara program client terhubung pada program server database. Pertama-tama, di-_include_ library yang dibutuhkan sebagai berikut.
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
```
Kemudian di-_define_ port yang akan digunakan pada _socket programming_ dan buffer_size untuk ukuran maksimal message yang akan dikomunikasikan antar program.
```
#define PORT 8080
#define BUFFER_SIZE 1024
```
Di dalam fungsi ```main()```, dideklarasikan dan diinisialisasikan variabel-variabel yang dibutuhkan untuk menjalankan _socket_ sebagai berikut.
```
int sock = 0;
    struct sockaddr_in serv_addr;
    char message[BUFFER_SIZE] = {0};
    char buffer[BUFFER_SIZE] = {0};

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
        perror("Invalid address/Address not supported");
        exit(EXIT_FAILURE);
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        perror("Connection failed");
        exit(EXIT_FAILURE);
    }
```
Potongan kode di atas membuat _socket file descriptor_, menampung alamat server dalam ```serv_addr```, lalu menginisialisasi ```array of characters``` sebagai nol. Kemudian dibuat socket dengan fungsi ```socker()``` dan menghubungkan program dengan server dengan fungsi ```connect()```. Kedua hal tersebut disertai _error handling_.

Setelah itu, program masuk ke dalam _infinite loop_ yang terus menerima pesan dari ```console```, mengirimkannya pada server, lalu menerima respons dari server. _Looping_ tersebut dilakukan supaya program tidak langsung berhenti setelah menerima respons dari server.

```
    printf("client-service:> ");
    fgets(message, BUFFER_SIZE, stdin);

    message[strcspn(message, "\n")] = '\0';

    if (strcmp(message, "exit") == 0)
        break;

    if (send(sock, message, strlen(message), 0) < 0) {
        perror("Send failed");
        exit(EXIT_FAILURE);
    }
    printf("Message sent to the server\n");

    if (read(sock, buffer, BUFFER_SIZE) < 0) {
        perror("Read failed");
        exit(EXIT_FAILURE);
    }
    printf("Server response: %s\n", buffer);
```
Terakhir, dipanggil fungsi ```close(sock)``` untuk menutup koneksi _socket_.

# Program Database
Pertama-tama, di-_include_ library yang diperlukan dan di-_define_ variabel konstan sebagai berikut.
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>

#define PORT 8080
#define BUFFER_SIZE 1024
#define MAX_CLIENTS 10
#define MAX_FIELDS 10
#define FIELD_SIZE 20
#define MAX_NAME_LENGTH 50
#define MAX_ATTRIBUTE_LENGTH 50
#define MAX_RECORD_LENGTH 500
```
Pertama, dideklarasikan fungsi untuk membuat database baru.
```
bool __createDatabase(const char *databaseName) {
    if (mkdir(databaseName, 0777) == -1) {
        return false;
    }

    return true;
}

bool createDatabase(char *databaseName) {
    return __createDatabase(databaseName);
}
```
Kemudian fungsi untuk membuat tabel baru
```
bool __createTable(const char *databaseName, const char *tableName, struct Attribute *attributes, int numAttributes) {
    char path[MAX_NAME_LENGTH + 1 + MAX_NAME_LENGTH + 1];
    sprintf(path, "%s/%s", databaseName, tableName);

    FILE *file = fopen(path, "w");
    if (file == NULL) {
        return false;
    }

    for (int i = 0; i < numAttributes; i++) {
        fprintf(file, "%s:%s;", attributes[i].name, attributes[i].type);
    }
    fprintf(file, "\n");

    fclose(file);

    return true;
}

bool createTable(char *databaseName, char *tableName, char *attributes) {
    //  parse attributes
    char copy[strlen(attributes) + 1];
    strcpy(copy, attributes);

    //  Initialize fields and number of fields
    int numAttributes = 0;
    struct Attribute attributeList[MAX_FIELDS];
    memset(attributeList, 0, sizeof(struct Attribute) * MAX_FIELDS);

    //  Tokenize the message using space as the delimiter
    char *token = strtok(copy, ":");
    while (token != NULL && numAttributes < MAX_FIELDS) {
        strcpy(attributeList[numAttributes].name, token);
        token = strtok(NULL, ",");
        strcpy(attributeList[numAttributes].type, token);
        token = strtok(NULL, ":");
        numAttributes++;
    }

    return __createTable(databaseName, tableName, attributeList, numAttributes);
}
```
Kemudian, fungsi untuk meng-insert record ke dalam tabel
```
bool __insertRecord(const char *databaseName, const char *tableName, int numAttributes, char **recordData) {
    char path[MAX_NAME_LENGTH + 1 + MAX_NAME_LENGTH + 1];
    sprintf(path, "%s/%s", databaseName, tableName);

    FILE *file = fopen(path, "a");
    printf("File: %s\n", path);
    printf("Num attributes: %d\n", numAttributes);
    if (file == NULL) {
        return false;
    }

    for (int i = 0; i < numAttributes; i++) {
        printf("Record data: %s\n", recordData[i]);
        fprintf(file, "%s;", recordData[i]);
    }
    fprintf(file, "\n");

    fclose(file);

    return true;
}

bool insertRecord(char *databaseName, char *tableName, char *recordData) {
    char *token;
    char copy[strlen(recordData) + 1];
    strcpy(copy, recordData);

    int numAttributes = 0;
    char recordList[MAX_FIELDS][FIELD_SIZE];
    memset(recordList, 0, sizeof(char) * MAX_FIELDS * FIELD_SIZE);

    token = strtok(copy, ",");
    while (token != NULL && numAttributes < MAX_FIELDS) {
        strcpy(recordList[numAttributes], token);
        numAttributes++;
        printf("%s\n", recordList[numAttributes]);
        if (token = strtok(NULL, ",") == NULL) {
            break;
        }
    }

    return __insertRecord(databaseName, tableName, numAttributes, recordList);
}
```
Lalu fungsi untuk membaca semua record dalam sebuah tabel
```
void readRecords(const char *databaseName, const char *tableName, struct Attribute *attributes, int numAttributes) {
    char path[MAX_NAME_LENGTH + 1 + MAX_NAME_LENGTH + 1];
    sprintf(path, "%s/%s", databaseName, tableName);

    FILE *file = fopen(path, "r");
    if (file == NULL) {
        perror("Failed to open the table file");
        exit(EXIT_FAILURE);
    }

    char attributeLine[MAX_RECORD_LENGTH];
    if (fgets(attributeLine, sizeof(attributeLine), file) == NULL) {
        perror("Failed to read the attribute line");
        exit(EXIT_FAILURE);
    }

    char recordLine[MAX_RECORD_LENGTH];
    while (fgets(recordLine, sizeof(recordLine), file) != NULL) {
        char *token = strtok(recordLine, ";");
        int attributeIndex = 0;

        printf("Record:\n");
        while (token != NULL && attributeIndex < numAttributes) {
            printf("%s: %s\n", attributes[attributeIndex].name, token);
            token = strtok(NULL, ";");
            attributeIndex++;
        }
        printf("\n");
    }

    fclose(file);
}
```
Lalu fungsi untuk melakukan select pada record dalam tabel
```
bool __selectRecords(const char *databaseName, const char *tableName, struct Attribute *attributes, int numAttributes, const char *attributeName, const char *attributeValue) {
    char path[MAX_NAME_LENGTH + 1 + MAX_NAME_LENGTH + 1];
    sprintf(path, "%s/%s", databaseName, tableName);

    FILE *file = fopen(path, "r");
    if (file == NULL) {
        return false;
    }

    // Read and discard the attribute line
    char attributeLine[MAX_RECORD_LENGTH];
    if (fgets(attributeLine, sizeof(attributeLine), file) == NULL) {
        return false;
    }

    // Search for records with the specified attribute value
    char recordLine[MAX_RECORD_LENGTH];
    int recordFound = 0;
    while (fgets(recordLine, sizeof(recordLine), file) != NULL) {
        char *token = strtok(recordLine, ";");
        int attributeIndex = 0;

        while (token != NULL && attributeIndex < numAttributes) {
            if (strcmp(attributes[attributeIndex].name, attributeName) == 0 && strcmp(token, attributeValue) == 0) {
                printf("Record:\n");
                int i;
                for (i = 0; i < numAttributes; i++) {
                    printf("%s: %s\n", attributes[i].name, token);
                    token = strtok(NULL, ";");
                }
                printf("\n");

                recordFound = 1;
                break;
            }

            token = strtok(NULL, ";");
            attributeIndex++;
        }

        if (recordFound) {
            break;
        }
    }

    if (!recordFound) {
        return false;
    }

    fclose(file);

    return true;
}

bool selectRecords(char *databaseName, char *tableName, char *attributeName, char *attributeValue) {
    //  parse attributes
    char *token;
    char copy[strlen(attributeName) + 1];
    strcpy(copy, attributeName);

    //  Initialize fields and number of fields
    int numAttributes = 0;
    struct Attribute attributeList[MAX_FIELDS];
    memset(attributeList, 0, sizeof(struct Attribute) * MAX_FIELDS);

    //  Tokenize the message using space as the delimiter
    token = strtok(copy, ",");
    while (token != NULL && numAttributes < MAX_FIELDS) {
        strcpy(attributeList[numAttributes].name, token);
        token = strtok(NULL, ",");
        strcpy(attributeList[numAttributes].type, token);
        token = strtok(NULL, ",");
        numAttributes++;
    }

    return __selectRecords(databaseName, tableName, attributeList, numAttributes, attributeName, attributeValue);
}
```
Kemudian fungsi untuk menghapus database dan menghabpus tabel
```
bool __dropDatabase(const char *databaseName) {
    if (remove(databaseName) == -1) {
        return false;
    }
    
    return true;
}

bool __dropTable(const char *databaseName, const char *tableName) {
    char path[MAX_NAME_LENGTH + 1 + MAX_NAME_LENGTH + 1];
    sprintf(path, "%s/%s", databaseName, tableName);

    if (remove(path) == -1) {
        return false;
    }
    
    return true;
}

bool dropDatabase(char *databaseName) {
    bool __dropDatabase(databaseName);
}

bool dropTable(char *databaseName, char *tableName) {
    return __dropTable(databaseName, tableName);
}
```
Kemudian dalam fungsi ```main()```, dibuat juga socket yang melakukan _listen_ pada _message_ sesuai port yang telah ditentukan.
```
int server_fd, new_socket, client_sockets[MAX_CLIENTS], max_socket, activity, i, valread, sd;
    int opt = 1;
    int addrlen;
    struct sockaddr_in address;
    char buffer[BUFFER_SIZE] = {0};
    char *response = "Hello from server";

    // Set of socket descriptors
    fd_set readfds;

    // Initialize client sockets array
    for (i = 0; i < MAX_CLIENTS; i++) {
        client_sockets[i] = 0;
    }

    // Create socket file descriptor
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

    // Attach socket to the port
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("Setsockopt failed");
        exit(EXIT_FAILURE);
    }
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    // Bind the socket to the specified IP and port
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("Bind failed");
        exit(EXIT_FAILURE);
    }

    // Listen for incoming connections
    if (listen(server_fd, 3) < 0) {
        perror("Listen failed");
        exit(EXIT_FAILURE);
    }

    addrlen = sizeof(address);
```
Lalu, dibuat sebuah _infinite loop_ yang melakukan _logic_ sesuai perintah dari _client_.
```
while (1) {
        // Clear the socket set
        FD_ZERO(&readfds);

        // Add the server socket to the set
        FD_SET(server_fd, &readfds);
        max_socket = server_fd;

        // Add client sockets to the set
        for (i = 0; i < MAX_CLIENTS; i++) {
            sd = client_sockets[i];

            if (sd > 0)
                FD_SET(sd, &readfds);

            if (sd > max_socket)
                max_socket = sd;
        }

        // Monitor the sockets for activity
        activity = select(max_socket + 1, &readfds, NULL, NULL, NULL);
        if ((activity < 0) && (errno != EINTR)) {
            printf("Select error");
        }

        // If the server socket has activity, it means a new connection
        if (FD_ISSET(server_fd, &readfds)) {
            if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) {
                perror("Accept failed");
                exit(EXIT_FAILURE);
            }

            // Add new client socket to the array
            for (i = 0; i < MAX_CLIENTS; i++) {
                if (client_sockets[i] == 0) {
                    client_sockets[i] = new_socket;
                    break;
                }
            }
        }

        //  database name string
        char databaseName[MAX_NAME_LENGTH];

        // Handle client requests
        for (i = 0; i < MAX_CLIENTS; i++) {
            sd = client_sockets[i];

            if (FD_ISSET(sd, &readfds)) {
                if ((valread = read(sd, buffer, BUFFER_SIZE)) == 0) {
                    // Client disconnected
                    close(sd);
                    client_sockets[i] = 0;
                } else {
                    // Parse the message
                    char fields[MAX_FIELDS][FIELD_SIZE];
                    int numFields;
                    parseMessage(buffer, fields, &numFields);

                    //  handle request based on the first field
                    if (strcmp(fields[0], "CREATE") == 0) {
                        if (strcmp(fields[1], "DATABASE") == 0) {
                            if (createDatabase(fields[2])) {
                                send(sd, "Successfully created database", strlen(response), 0);
                            } else {
                                send(sd, "Failed to create database", strlen(response), 0);
                            }
                        } else if (strcmp(fields[1], "TABLE") == 0) {
                            if (createTable(databaseName, fields[2], fields[3])) {
                                send(sd, "Successfully created table", strlen(response), 0);
                            } else {
                                send(sd, "Failed to create table", strlen(response), 0);
                            }
                        }
                    } else if (strcmp(fields[0], "USE") == 0) {
                        strcpy(databaseName, fields[1]);
                        send(sd, "Using database", strlen(response), 0);
                    } else if (strcmp(fields[0], "INSERT") == 0) {
                        if (insertRecord(databaseName, fields[2], fields[4])) {
                            send(sd, "Successfully inserted record", strlen(response), 0);
                        } else {
                            send(sd, "Failed to insert record", strlen(response), 0);
                        }
                    } else if (strcmp(fields[0], "SELECT") == 0) {
                        if (selectRecords(databaseName, fields[1], fields[2], fields[3])) {
                            send(sd, "Successfully selected records", strlen(response), 0);
                        } else {
                            send(sd, "Failed to select records", strlen(response), 0);
                        }
                    } else if (strcmp(fields[0], "DROP") == 0) {
                        if (fields[1] == "DATABASE") {
                            if (dropDatabase(fields[2])) {
                                send(sd, "Successfully dropped database", strlen(response), 0);
                            } else {
                                send(sd, "Failed to drop database", strlen(response), 0);
                            }
                        } else if (strcmp(fields[1], "TABLE") == 0) {
                            if (dropTable(databaseName, fields[2])) {
                                send(sd, "Successfully dropped table", strlen(response), 0);
                            } else {
                                send(sd, "Failed to drop table", strlen(response), 0);
                            }
                        }
                    } else if (strcmp(fields[0], "UPDATE") == 0) {
                        if (updateRecord(databaseName, fields[1], fields[2], fields[3], fields[4], fields[5])) {
                            send(sd, "Successfully updated record", strlen(response), 0);
                        } else {
                            send(sd, "Failed to update record", strlen(response), 0);
                        }
                    }

                    memset(buffer, 0, sizeof(buffer));
                }
            }
        }
    }
```

# Dockerfile
```
# Use the Ubuntu 20.04 base image
FROM ubuntu:20.04

# Set the working directory inside the container
WORKDIR /database_app

# Copy all directories and files into workdir
COPY client ./client
COPY database ./database
COPY dump ./dump

# Update the package manager and install any necessary dependencies
RUN apt-get update && apt-get install -y build-essential

# Compile the C programs
RUN gcc -o client_exe client/client.c
RUN gcc -pthread -o database_exe database/database.c

# Expose the necessary port for socket communication
EXPOSE 8080

# Run the database program
CMD [ "./database_exe" ]

# Set the entrypoint to run the compiled client program
ENTRYPOINT ["./client_exe"]
```
Konten isi Dockerfile tersebut mmengambil image Ubuntu versi 20.04, kemudian mengatur direktori pengerjaan di ```/database_app```. Kemudian menyalin program-program yang ada di _filesystem_ lokal ke dalam _container_. Setelah itu diinstall _package_ yang diperlukan untuk menjalankan program. Kemudian program ```client``` dan ```database``` di-_compile_. Lalu di-expose ke port 8080, dan di-set program ```client``` sebagai _entry point_ dari _container_.

# Kendala Pengerjaan
Timeline pengerjaan yang bersamaan dengan _final project_ dari matkul-matkul lain, sulitnya menghubungi teman kelompok, dan kesibukan diluar akademik.