#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define PORT 8080
#define BUFFER_SIZE 1024

int main() {
    int sock = 0;
    struct sockaddr_in serv_addr;
    char message[BUFFER_SIZE] = {0};
    char buffer[BUFFER_SIZE] = {0};

    // Create socket file descriptor
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    // Convert IPv4 and IPv6 addresses from text to binary form
    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
        perror("Invalid address/Address not supported");
        exit(EXIT_FAILURE);
    }

    // Connect to the server
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        perror("Connection failed");
        exit(EXIT_FAILURE);
    }

    while (1) {
        // Prompt the user to enter a message
        printf("client-service:> ");
        fgets(message, BUFFER_SIZE, stdin);

        // Remove the trailing newline character
        message[strcspn(message, "\n")] = '\0';

        // Check if the user wants to quit
        if (strcmp(message, "exit") == 0)
            break;

        // Send message to the server
        if (send(sock, message, strlen(message), 0) < 0) {
            perror("Send failed");
            exit(EXIT_FAILURE);
        }
        printf("Message sent to the server\n");

        // Receive response from the server
        if (read(sock, buffer, BUFFER_SIZE) < 0) {
            perror("Read failed");
            exit(EXIT_FAILURE);
        }
        printf("Server response: %s\n", buffer);
    }

    close(sock);

    return 0;
}
