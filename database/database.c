#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>

#define PORT 8080
#define BUFFER_SIZE 1024
#define MAX_CLIENTS 10
#define MAX_FIELDS 10
#define FIELD_SIZE 20
#define MAX_NAME_LENGTH 50
#define MAX_ATTRIBUTE_LENGTH 50
#define MAX_RECORD_LENGTH 500

struct Attribute {
    char name[MAX_ATTRIBUTE_LENGTH];
    char type[MAX_ATTRIBUTE_LENGTH];
};

bool __createDatabase(const char *databaseName) {
    if (mkdir(databaseName, 0777) == -1) {
        return false;
    }

    return true;
}

bool __createTable(const char *databaseName, const char *tableName, struct Attribute *attributes, int numAttributes) {
    char path[MAX_NAME_LENGTH + 1 + MAX_NAME_LENGTH + 1];
    sprintf(path, "%s/%s", databaseName, tableName);

    FILE *file = fopen(path, "w");
    if (file == NULL) {
        return false;
    }

    for (int i = 0; i < numAttributes; i++) {
        fprintf(file, "%s:%s;", attributes[i].name, attributes[i].type);
    }
    fprintf(file, "\n");

    fclose(file);

    return true;
}

bool __insertRecord(const char *databaseName, const char *tableName, int numAttributes, char **recordData) {
    char path[MAX_NAME_LENGTH + 1 + MAX_NAME_LENGTH + 1];
    sprintf(path, "%s/%s", databaseName, tableName);

    FILE *file = fopen(path, "a");
    printf("File: %s\n", path);
    printf("Num attributes: %d\n", numAttributes);
    if (file == NULL) {
        return false;
    }

    for (int i = 0; i < numAttributes; i++) {
        printf("Record data: %s\n", recordData[i]);
        fprintf(file, "%s;", recordData[i]);
    }
    fprintf(file, "\n");

    fclose(file);

    return true;
}

void readRecords(const char *databaseName, const char *tableName, struct Attribute *attributes, int numAttributes) {
    char path[MAX_NAME_LENGTH + 1 + MAX_NAME_LENGTH + 1];
    sprintf(path, "%s/%s", databaseName, tableName);

    FILE *file = fopen(path, "r");
    if (file == NULL) {
        perror("Failed to open the table file");
        exit(EXIT_FAILURE);
    }

    // Read and discard the attribute line
    char attributeLine[MAX_RECORD_LENGTH];
    if (fgets(attributeLine, sizeof(attributeLine), file) == NULL) {
        perror("Failed to read the attribute line");
        exit(EXIT_FAILURE);
    }

    // Read and print the records
    char recordLine[MAX_RECORD_LENGTH];
    while (fgets(recordLine, sizeof(recordLine), file) != NULL) {
        char *token = strtok(recordLine, ";");
        int attributeIndex = 0;

        printf("Record:\n");
        while (token != NULL && attributeIndex < numAttributes) {
            printf("%s: %s\n", attributes[attributeIndex].name, token);
            token = strtok(NULL, ";");
            attributeIndex++;
        }
        printf("\n");
    }

    fclose(file);
}

bool __selectRecords(const char *databaseName, const char *tableName, struct Attribute *attributes, int numAttributes, const char *attributeName, const char *attributeValue) {
    char path[MAX_NAME_LENGTH + 1 + MAX_NAME_LENGTH + 1];
    sprintf(path, "%s/%s", databaseName, tableName);

    FILE *file = fopen(path, "r");
    if (file == NULL) {
        return false;
    }

    // Read and discard the attribute line
    char attributeLine[MAX_RECORD_LENGTH];
    if (fgets(attributeLine, sizeof(attributeLine), file) == NULL) {
        return false;
    }

    // Search for records with the specified attribute value
    char recordLine[MAX_RECORD_LENGTH];
    int recordFound = 0;
    while (fgets(recordLine, sizeof(recordLine), file) != NULL) {
        char *token = strtok(recordLine, ";");
        int attributeIndex = 0;

        while (token != NULL && attributeIndex < numAttributes) {
            if (strcmp(attributes[attributeIndex].name, attributeName) == 0 && strcmp(token, attributeValue) == 0) {
                printf("Record:\n");
                int i;
                for (i = 0; i < numAttributes; i++) {
                    printf("%s: %s\n", attributes[i].name, token);
                    token = strtok(NULL, ";");
                }
                printf("\n");

                recordFound = 1;
                break;
            }

            token = strtok(NULL, ";");
            attributeIndex++;
        }

        if (recordFound) {
            break;
        }
    }

    if (!recordFound) {
        return false;
    }

    fclose(file);

    return true;
}

bool __dropDatabase(const char *databaseName) {
    if (remove(databaseName) == -1) {
        return false;
    }
    
    return true;
}

bool __dropTable(const char *databaseName, const char *tableName) {
    char path[MAX_NAME_LENGTH + 1 + MAX_NAME_LENGTH + 1];
    sprintf(path, "%s/%s", databaseName, tableName);

    if (remove(path) == -1) {
        return false;
    }
    
    return true;
}

bool __updateRecord(const char *databaseName, const char *tableName, struct Attribute *attributes, int numAttributes, int recordIndex, const char *newValue) {
    char path[MAX_NAME_LENGTH + 1 + MAX_NAME_LENGTH + 1];
    sprintf(path, "%s/%s", databaseName, tableName);

    FILE *file = fopen(path, "r+");
    if (file == NULL) {
        return false;
    }

    // Read and discard the attribute line
    char attributeLine[MAX_RECORD_LENGTH];
    if (fgets(attributeLine, sizeof(attributeLine), file) == NULL) {
        return false;
    }

    // Find the desired record and update its value
    char recordLine[MAX_RECORD_LENGTH];
    int currentRecordIndex = 0;
    while (fgets(recordLine, sizeof(recordLine), file) != NULL) {
        if (currentRecordIndex == recordIndex) {
            // Update the value of the record
            fseek(file, -strlen(recordLine), SEEK_CUR);
            fprintf(file, "%s", newValue);
            fflush(file);
            break;
        }
        currentRecordIndex++;
    }

    fclose(file);

    return true;
}

//  function for message parsing
void parseMessage(const char *message, char fields[MAX_FIELDS][FIELD_SIZE], int *numFields) {
    char *token;
    char copy[strlen(message) + 1];
    strcpy(copy, message);

    // Initialize fields and number of fields
    *numFields = 0;
    memset(fields, 0, sizeof(char) * MAX_FIELDS * FIELD_SIZE);

    // Tokenize the message using space as the delimiter
    token = strtok(copy, " ");
    while (token != NULL && *numFields < MAX_FIELDS) {
        strcpy(fields[*numFields], token);
        (*numFields)++;
        token = strtok(NULL, " ");
    }
}

//  function wrapper for case create database
bool createDatabase(char *databaseName) {
    return __createDatabase(databaseName);
}

//  function wrapper for case create table
bool createTable(char *databaseName, char *tableName, char *attributes) {
    //  parse attributes
    char copy[strlen(attributes) + 1];
    strcpy(copy, attributes);

    //  Initialize fields and number of fields
    int numAttributes = 0;
    struct Attribute attributeList[MAX_FIELDS];
    memset(attributeList, 0, sizeof(struct Attribute) * MAX_FIELDS);

    //  Tokenize the message using space as the delimiter
    char *token = strtok(copy, ":");
    while (token != NULL && numAttributes < MAX_FIELDS) {
        strcpy(attributeList[numAttributes].name, token);
        token = strtok(NULL, ",");
        strcpy(attributeList[numAttributes].type, token);
        token = strtok(NULL, ":");
        numAttributes++;
    }

    return __createTable(databaseName, tableName, attributeList, numAttributes);
}

//  function wrapper for case insert
bool insertRecord(char *databaseName, char *tableName, char *recordData) {
    //  parse record data
    char *token;
    char copy[strlen(recordData) + 1];
    strcpy(copy, recordData);

    //  Initialize fields and number of fields
    int numAttributes = 0;
    char recordList[MAX_FIELDS][FIELD_SIZE];
    memset(recordList, 0, sizeof(char) * MAX_FIELDS * FIELD_SIZE);

    //  Tokenize the message using space as the delimiter
    token = strtok(copy, ",");
    while (token != NULL && numAttributes < MAX_FIELDS) {
        strcpy(recordList[numAttributes], token);
        numAttributes++;
        printf("%s\n", recordList[numAttributes]);
        if (token = strtok(NULL, ",") == NULL) {
            break;
        }
    }

    return __insertRecord(databaseName, tableName, numAttributes, recordList);
}

//  function wrapper for case select
bool selectRecords(char *databaseName, char *tableName, char *attributeName, char *attributeValue) {
    //  parse attributes
    char *token;
    char copy[strlen(attributeName) + 1];
    strcpy(copy, attributeName);

    //  Initialize fields and number of fields
    int numAttributes = 0;
    struct Attribute attributeList[MAX_FIELDS];
    memset(attributeList, 0, sizeof(struct Attribute) * MAX_FIELDS);

    //  Tokenize the message using space as the delimiter
    token = strtok(copy, ",");
    while (token != NULL && numAttributes < MAX_FIELDS) {
        strcpy(attributeList[numAttributes].name, token);
        token = strtok(NULL, ",");
        strcpy(attributeList[numAttributes].type, token);
        token = strtok(NULL, ",");
        numAttributes++;
    }

    return __selectRecords(databaseName, tableName, attributeList, numAttributes, attributeName, attributeValue);
}

//  function wrapper for case drop database
bool dropDatabase(char *databaseName) {
    bool __dropDatabase(databaseName);
}

//  function wrapper for case drop table
bool dropTable(char *databaseName, char *tableName) {
    return __dropTable(databaseName, tableName);
}

//  function wrapper for case update record
bool updateRecord(char *databaseName, char *tableName, char *attributeName, char *attributeValue, int recordIndex, char *newValue) {
    return __updateRecord(databaseName, tableName, NULL, 0, recordIndex, newValue);
}

int main() {
    int server_fd, new_socket, client_sockets[MAX_CLIENTS], max_socket, activity, i, valread, sd;
    int opt = 1;
    int addrlen;
    struct sockaddr_in address;
    char buffer[BUFFER_SIZE] = {0};
    char *response = "Hello from server";

    // Set of socket descriptors
    fd_set readfds;

    // Initialize client sockets array
    for (i = 0; i < MAX_CLIENTS; i++) {
        client_sockets[i] = 0;
    }

    // Create socket file descriptor
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

    // Attach socket to the port
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("Setsockopt failed");
        exit(EXIT_FAILURE);
    }
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    // Bind the socket to the specified IP and port
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("Bind failed");
        exit(EXIT_FAILURE);
    }

    // Listen for incoming connections
    if (listen(server_fd, 3) < 0) {
        perror("Listen failed");
        exit(EXIT_FAILURE);
    }

    addrlen = sizeof(address);

    while (1) {
        // Clear the socket set
        FD_ZERO(&readfds);

        // Add the server socket to the set
        FD_SET(server_fd, &readfds);
        max_socket = server_fd;

        // Add client sockets to the set
        for (i = 0; i < MAX_CLIENTS; i++) {
            sd = client_sockets[i];

            if (sd > 0)
                FD_SET(sd, &readfds);

            if (sd > max_socket)
                max_socket = sd;
        }

        // Monitor the sockets for activity
        activity = select(max_socket + 1, &readfds, NULL, NULL, NULL);
        if ((activity < 0) && (errno != EINTR)) {
            printf("Select error");
        }

        // If the server socket has activity, it means a new connection
        if (FD_ISSET(server_fd, &readfds)) {
            if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) {
                perror("Accept failed");
                exit(EXIT_FAILURE);
            }

            // Add new client socket to the array
            for (i = 0; i < MAX_CLIENTS; i++) {
                if (client_sockets[i] == 0) {
                    client_sockets[i] = new_socket;
                    break;
                }
            }
        }

        //  database name string
        char databaseName[MAX_NAME_LENGTH];

        // Handle client requests
        for (i = 0; i < MAX_CLIENTS; i++) {
            sd = client_sockets[i];

            if (FD_ISSET(sd, &readfds)) {
                if ((valread = read(sd, buffer, BUFFER_SIZE)) == 0) {
                    // Client disconnected
                    close(sd);
                    client_sockets[i] = 0;
                } else {
                    // Parse the message
                    char fields[MAX_FIELDS][FIELD_SIZE];
                    int numFields;
                    parseMessage(buffer, fields, &numFields);

                    //  handle request based on the first field
                    if (strcmp(fields[0], "CREATE") == 0) {
                        if (strcmp(fields[1], "DATABASE") == 0) {
                            if (createDatabase(fields[2])) {
                                send(sd, "Successfully created database", strlen(response), 0);
                            } else {
                                send(sd, "Failed to create database", strlen(response), 0);
                            }
                        } else if (strcmp(fields[1], "TABLE") == 0) {
                            if (createTable(databaseName, fields[2], fields[3])) {
                                send(sd, "Successfully created table", strlen(response), 0);
                            } else {
                                send(sd, "Failed to create table", strlen(response), 0);
                            }
                        }
                    } else if (strcmp(fields[0], "USE") == 0) {
                        strcpy(databaseName, fields[1]);
                        send(sd, "Using database", strlen(response), 0);
                    } else if (strcmp(fields[0], "INSERT") == 0) {
                        if (insertRecord(databaseName, fields[2], fields[4])) {
                            send(sd, "Successfully inserted record", strlen(response), 0);
                        } else {
                            send(sd, "Failed to insert record", strlen(response), 0);
                        }
                    } else if (strcmp(fields[0], "SELECT") == 0) {
                        if (selectRecords(databaseName, fields[1], fields[2], fields[3])) {
                            send(sd, "Successfully selected records", strlen(response), 0);
                        } else {
                            send(sd, "Failed to select records", strlen(response), 0);
                        }
                    } else if (strcmp(fields[0], "DROP") == 0) {
                        if (fields[1] == "DATABASE") {
                            if (dropDatabase(fields[2])) {
                                send(sd, "Successfully dropped database", strlen(response), 0);
                            } else {
                                send(sd, "Failed to drop database", strlen(response), 0);
                            }
                        } else if (strcmp(fields[1], "TABLE") == 0) {
                            if (dropTable(databaseName, fields[2])) {
                                send(sd, "Successfully dropped table", strlen(response), 0);
                            } else {
                                send(sd, "Failed to drop table", strlen(response), 0);
                            }
                        }
                    } else if (strcmp(fields[0], "UPDATE") == 0) {
                        if (updateRecord(databaseName, fields[1], fields[2], fields[3], fields[4], fields[5])) {
                            send(sd, "Successfully updated record", strlen(response), 0);
                        } else {
                            send(sd, "Failed to update record", strlen(response), 0);
                        }
                    }

                    memset(buffer, 0, sizeof(buffer));
                }
            }
        }
    }

    return 0;
}
